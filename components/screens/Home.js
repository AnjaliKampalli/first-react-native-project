import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StatusBar,
  ScrollView,
  TouchableOpacity,
  TouchableHighlight,
  Image,
  Button,
  StyleSheet
} from 'react-native';
import {COLORS, Items, buttons} from '../database/Database';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Icon from 'react-native-vector-icons/MaterialIcons';
import categories from '../constants/categories';

const Home = ({navigation}) => {
  const [selectedCategoryIndex, setSelectedCategoryIndex ]  = React.useState(0);

  const ListCategories = () => {
    return (
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={style.categoriesListContainer}>
        {categories.map((category, index) => (
          <TouchableOpacity
            key={index}
            activeOpacity={0.8}
            onPress={() => setSelectedCategoryIndex(index)}>
            <View
              style={{
                backgroundColor:
                  selectedCategoryIndex == index
                    ? COLORS.white
                    : COLORS.white,
                ...style.categoryBtn,
              }}>
              <View style={style.categoryBtnImgCon}>
                <Image
                  source={category.image}
                  style={{height: 35, width: 35, resizeMode: 'cover'}}
                />
              </View>
              <Text
                style={{
                  fontSize: 15,
                  fontWeight: 'bold',
                  marginLeft: 10,
                  color:
                    selectedCategoryIndex == index
                      ? COLORS.blue
                      : COLORS.black,
                }}>
                {category.name}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    );
  };

  const [products, setProducts] = useState([]);

  useEffect(() => {
    getDataFromDB();
  }, [navigation]);

  const getDataFromDB = () => {
    let productList = [];
    
    for (let index = 0; index < Items.length; index++) {
      if (Items[index].category == 'sneakers') {
        productList.push(Items[index]);
      } 
    }

    setProducts(productList);
  };

  // function handleProducts(e) {
  //   let productType = e.target.value;
  //   productType !== "all"
  //     ? setProducts(filterPokemon(productType))
  //     : setFiltredPokemon(getPokemon());
  // }

  const ProductCard = ({data}) => {
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('ProductInfo', {productID: data.id})}
        style={{
          width: '50%',
          marginVertical: 14,
          alignItems: 'center',
        }}>
        <View
          style={{
            width: '85%',
            height: 200,
            borderRadius: 10,
            backgroundColor: COLORS.white,
            position: 'relative',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 8,
          }}>
          {data.isOff ? (
            <View
              style={{
                // height: 45,
                // width: 120,
                // marginRight: 7,
                borderRadius: 30,
                alignItems: 'center',
                marginTop: 10,
                marginLeft: 10,
                // paddingHorizontal: 5,
                // flexDirection: 'row',

                position: 'absolute',
                width: '20%',
                height: '10%',
                backgroundColor: COLORS.skyBlue,
                top: 0,
                left: 0,
                borderTopLeftRadius: 10,
                borderBottomRightRadius: 10,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 10,
                  color: COLORS.white,
                  fontWeight: 'bold',
                  letterSpacing: 0,
                }}>
                {data.offPercentage}%
              </Text>
            </View>
          ) : null}
            <Entypo
          name='heart'
          style={{
            fontSize: 18,
            color: COLORS.red,
            marginLeft:'80%',
            // marginBottom: 2,
            marginTop: 20,
            // padding: 16,
            // marginRight: 7,
            borderRadius: 30,
            backgroundColor: COLORS.backgroundLight,
          }}
          />
          <Image
            source={data.productImage}
            style={{
              width: '70%',
              height: '70%',
              resizeMode: 'contain',
            }}
          />
        
        <Text
          style={style.productName}>
          {data.productName}
        </Text>
        <Text style={style.productPrice} >&#8377; {data.productPrice}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View
      style={{
        width: '100%',
        height: '100%',
        backgroundColor: COLORS.grey,

      }}>
      <StatusBar backgroundColor={COLORS.white} barStyle="dark-content" />
      <ScrollView  showsHorizontalScrollIndicator={false}>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: 16,
            
          }}>
          <TouchableOpacity>
            <Entypo
              name='menu'
              style={{
                fontSize: 18,
                color: COLORS.backgroundDark,
                padding: 12,
                borderRadius: 10,
              }}
            />
          </TouchableOpacity>
          <Text style={{
            fontSize: 18,
            color: COLORS.backgroundDark,
            padding: 12,
            borderRadius: 10,
            fontWeight:'bold',
            fontFamily: "Times New Roman"
          }}>
            XE
          </Text>
          <TouchableOpacity >
            <Icon
              name="search"
              style={{
                size : 28,
                fontSize: 18,
                // color: COLOURS.backgroundDark,
                padding: 12,
                // borderRadius: 10,
              }}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: 16,
          }}>
          <Text
            style={{
              fontSize: 26,
              color: COLORS.black,
              fontWeight: '500',
              letterSpacing: 1,
              marginBottom: 10,
            }}>
            Our Product
          </Text>
          <Text
            style={{
              fontSize: 14,
              color: COLORS.black,
              fontWeight: '400',
              letterSpacing: 1,
              marginBottom: 10,
            }}>
            Sort By 
          </Text>
        </View>
        <ListCategories></ListCategories>
        {/* <View   
        style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-between',
            backgroundColor: COLOURS.white,
            padding: 20,
            borderRadius: 10,

          }}>
            
            {buttons  &&
        buttons.map((type, index) => (
          <>
            <Button 
             key={index} value={type.value} title={type.name} onPress={getDataFromDB}>
            </Button>

          </>
        ))} 
        </View> */}
        <View
          style={{
            padding: 16,
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontSize: 18,
                  color: COLORS.black,
                  fontWeight: '500',
                  letterSpacing: 1,
                }}>
                Products
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  color: COLORS.black,
                  fontWeight: '400',
                  opacity: 0.5,
                  marginLeft: 10,
                }}>
                {Items.length}
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              justifyContent: 'space-around',
            }}>
            {products.map(data => {
              return <ProductCard data={data} key={data.id} />;
            })}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const style = StyleSheet.create({
  categoriesListContainer: {
    paddingVertical: 30,
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  categoryBtn: {
    height: 45,
    width: 120,
    marginRight: 7,
    borderRadius: 30,
    alignItems: 'center',
    paddingHorizontal: 5,
    flexDirection: 'row',
  },
  categoryBtnImgCon: {
    height: 35,
    width: 35,
    backgroundColor: COLORS.white,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  productPrice: {
    
      fontSize: 17,
      color: COLORS.blue,
      fontWeight: '600',
      marginBottom: 10,
      fontWeight: 'bold',

  },
  productName: {
    
    fontSize: 17,
    color: COLORS.blue,
    fontWeight: '600',
    marginBottom: 10,

}
});
export default Home;
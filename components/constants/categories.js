

  const categories = [
    {id: '1', name: 'sneakers', image: require('../database/images/sneakers/1.jpg')},
    {id: '2', name: 'watch', image: require('../database/images/watches/w1.jpg')},
    {id: '3', name: 'backpack', image: require('../database/images/backpack/b1.jpg')},
  ];
  
  export default categories;